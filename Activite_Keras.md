# Deep learning

L'objectif est de créer un modèle de deep learning simple pour répondre de problème de classification binaire du Titanic.

## Activité

- Récupérer le jeu de données du titanic : https://raw.githubusercontent.com/datasciencedojo/datasets/master/titanic.csv

- Créer un jeu de données d'entrainement comme dans l'activité sur la courbe ROC

- A l'aide de la librairie Keras, créer un modèle séquentiel de deep learning

- Ajouter plusieurs couches denses de neurones

- Vérifier que la couche d'entrée et la couche de sortie sont en accord avec les données d'entrainement

- Compiler et entrainer le modèle de deep learning

- Evaluer le modèle et comparer aux modèles de machine learning