# Courbe ROC

![roc-curve.png](roc-curve.png)

L'aire sous la courbe ROC (AUC) est utilisée pour mesurer la qualité du modèle de détection d'anomalies de la compétition Kaggle.

## Recherche d'informations

- A quoi correspond la courbe ROC ?

- De quel domaine scientifique est-elle issue ?

- A quoi correspond l'aire sous la courbe ROC (AUC) ?

- Quelle est la différence avec les autres métriques de classification ?

## Activité

- Récupérer le jeu de données du titanic : https://raw.githubusercontent.com/datasciencedojo/datasets/master/titanic.csv

- Faire une analyse exploratoire

- Choisir 2 ou 3 features utiles pour prédir la survie au naufrage

- Entrainer 3 modèles de machine learning différents parmi SVM, knn, decision tree, random forest, gradient boosting, logistic regression

- Afficher la matrice de confusion pour les différents modèles

- Afficher un tableau de comparaison des métriques (accuracy, precision, recall, F1-score) pour les différents modèles

- Tracer la courbe ROC pour les différents modèles

- Calculer l'aire sous la courbe ROC pour les différents modèles

- Conclure sur le meilleur modèle
