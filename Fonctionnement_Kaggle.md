# Explication du fonctionnement de Kaggle

## Compétition

Grace au code écrit dans l'activité sur la courbe ROC, vous allez valider votre modèle sur la plateforme Kaggle.

## Activité

- Créer un compte sur le site de Kaggle : https://www.kaggle.com/

- S'inscrire à la compétition du Titanic : https://www.kaggle.com/competitions/titanic

- Aller à l'onglet "Code" et cliquer sur "New Notebook"

- Copier le code que vous avez écrit pour entrainer vos modèles pour l'activité sur la courbe ROC

- A la fin du Notebook, créer un fichier "submission.csv" à l'aide de la méthode `.to_csv("submission.csv", index=False)`

- Cliquer sur "Submit"

- Vérifier le bon fonctionnement du modèle (métrique et classement)