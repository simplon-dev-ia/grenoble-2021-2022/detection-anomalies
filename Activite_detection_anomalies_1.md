# Detection d'anomalies - Partie 1

![Courbe](courbe.png)

## Recherche d'information

- Comment fonctionne le modèle des K plus proches voisins pour la détection d'anomalies ?

- Comment fonctionne le modèle d'isolation forest par rapport à un modèle de Random Forest ?

- Quelle est la différence entre anomaly detection et novelty detection ?

- A quoi correspond la détection d'anomalies univariée ?

## Fonctionnement des compétitions Kaggle

L'objectif des compétitions est d'entrainer un modèle de machine learning à partir de données d'entrainement. Le classement des joueurs est réalisé à partir des résultats du modèle sur un jeu de données de test et avec une métrique d'évaluation spécifique à chaque compétition.

## Activité

Pour cette activité nous allons utiliser la compétition Kaggle suivante : https://www.kaggle.com/competitions/energy-anomaly-detection

Il s'agit de données de séries temporelles de capteurs de consommation électrique.

### Analyse exploratoire des données

- Télécharger les données depuis l'onglet "Data". Seuls les fichiers "sample_submission.csv", "train.csv" et "test.csv" nous intéressent

- Effectuer une analyse exploratoire des données d'entrainement ("train.csv")

- Mesurer le déséquilibre de classes

- Regrouper les données par batiment

- Effectuer une analyse univariée de détection de données aberrantes à l'aide du z-score (valeur supérieure à 3 en valeur absolue)

- Valider la qualité de cette détection d'anomalies à l'aide des données labellisées

- Effectuer une analyse univariée de détection de données aberrantes à l'aide de la distance interquartile (IQR). Visualiser le résultat à l'aide d'une boite à moustaches (box plot)

- Valider la qualité de cette détection d'anomalies à l'aide des données labellisées